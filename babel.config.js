module.exports = {
  exclude: 'node_modules/**',
  presets: [
    [
      'react-app',
      {
        flow: false,
        typescript: true,
      },
    ],
    [
      '@babel/preset-env',
      {
        targets: {
          node: '8.10',
        },
      },
    ],
  ],
}