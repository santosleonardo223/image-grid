import styled, { createGlobalStyle, css } from 'styled-components';

import theme from '@src/theme';

const AppGlobalStyles = createGlobalStyle`
  * {
    box-sizing: border-box;
  }

  body {
    margin: 0;
    padding: 0;
    -webkit-margin-before: 0;
    -webkit-margin-after: 0;
    background-color: ${theme.colors.lightGray};
  }
`;

const AppContainer = styled.div`
  ${({ theme }) => {
    return css`
      display: flex;
      flex-direction: column;
      height: 100vh;
      margin: auto;
      max-width: ${theme.screenBreakPoints.lg};

      @media screen and (max-width: ${theme.screenBreakPoints.sm}) {
        padding: ${theme.spacing.lg} ${theme.spacing.sm};
      }

      @media screen and (min-width: ${theme.screenBreakPoints.sm}) {
        padding: ${theme.spacing.lg} ${theme.spacing.xl};
      }
    `;
  }}
`;

export { AppGlobalStyles, AppContainer };
