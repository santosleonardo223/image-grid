import React from 'react';
import { ThemeProvider } from 'styled-components';

import theme from '@src/theme';
import { AppGlobalStyles, AppContainer } from '@src/styles';
import Router from '@src/Router';

const App = () => (
  <ThemeProvider theme={theme}>
    <AppGlobalStyles />
    <AppContainer>
      <Router />
    </AppContainer>
  </ThemeProvider>
);

export default App;
