import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import ImageGallery from '@src/pages/ImageGallery';

const Router = () => {
  return (
    <BrowserRouter>
      <Route exact path="/" component={() => <Redirect to="/images" />} />

      <Switch>
        <Route path="/images" component={ImageGallery} />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;
