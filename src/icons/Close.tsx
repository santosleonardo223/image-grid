import React from 'react';

import theme from '@src/theme/index';

const Close = ({
  onClick,
  width,
  fill,
  id,
}: {
  id?: string;
  onClick?: () => void;
  width?: string | number;
  fill?: string;
}) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    onClick={onClick}
    width={width || theme.widths.icon}
    fill={fill || 'inherit'}
    id={id}
  >
    <path d="M0 24h24V0H0z" fill="none" />
    <path d="M13.949 11.958L21.013 4.9a1.381 1.381 0 0 0 0-1.951 1.38 1.38 0 0 0-1.952 0L12 10.005 4.939 2.946A1.38 1.38 0 0 0 2.987 4.9l7.064 7.061-7.064 7.059a1.381 1.381 0 0 0 0 1.951 1.38 1.38 0 0 0 1.952 0L12 13.912l7.061 7.059a1.38 1.38 0 0 0 1.952 0 1.381 1.381 0 0 0 0-1.951z" />
  </svg>
);

export { Close };
