import React from 'react';

import theme from '@src/theme/index';

const CaretCircleDown = ({
  width,
  fill,
  onClick,
}: {
  width?: string | number;
  fill?: string;
  onClick?: () => void;
}) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    width={width || theme.widths.icon}
    fill={fill}
    onClick={onClick}
  >
    <path d="M0 24h24V0H0z" fill="none" />
    <path
      d="M12 1.958a10 10 0 1010 10 10 10 0 00-10-10zm5.15 8.642l-4.784 4.784a.518.518 0 01-.732 0L6.85 10.6a.621.621 0 01.439-1.06h9.422a.621.621 0 01.439 1.06z"
      fillRule="evenodd"
    />
  </svg>
);

export { CaretCircleDown };
