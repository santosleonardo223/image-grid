import styled, { css } from 'styled-components';
import { Masonry } from 'masonic';

import theme from '@src/theme/index';

const ImageGridInfiniteList = styled(Masonry)`
  display: grid;

  ${({ theme }) => css`
    box-sizing: border-box;
    width: 100%;
    margin-top: ${theme.spacing.lg};

    @media screen and (max-width: ${theme.screenBreakPoints.xs}) {
      grid-template-columns: 1fr;
    }

    @media screen and (min-width: ${theme.screenBreakPoints
        .xs}) and (max-width: ${theme.screenBreakPoints.sm}) {
      grid-template-columns: repeat(2, 1fr);
    }

    @media screen and (min-width: ${theme.screenBreakPoints
        .sm}) and (max-width: ${theme.screenBreakPoints.md}) {
      grid-template-columns: repeat(3, 1fr);
    }

    @media screen and (min-width: ${theme.screenBreakPoints.md}) {
      grid-template-columns: repeat(4, 1fr);
    }

    img {
      display: block;
      width: 100%;
      height: auto;
      padding: ${({ theme }) => theme.spacing.xs};
    }
  `}
`;

const arrowWidth = `calc(3 * ${theme.widths.icon})`;
const ArrowLeft = styled.div`
  position: fixed;
  top: calc((100vh / 2) - (${arrowWidth} / 2));
  left: 0;
  width: ${arrowWidth};
  height: ${arrowWidth};
  cursor: pointer;

  svg {
    transform: rotate(90deg);
    width: ${arrowWidth};
    fill: ${({ theme }) => theme.colors.white};
  }
`;

const ArrowRight = styled.div`
  position: fixed;
  top: calc((100vh / 2) - (${arrowWidth} / 2));
  right: 0;
  width: ${arrowWidth};
  height: ${arrowWidth};
  cursor: pointer;

  svg {
    transform: rotate(-90deg);
    width: ${arrowWidth};
    fill: ${({ theme }) => theme.colors.white};
  }
`;

export { ImageGridInfiniteList, ArrowLeft, ArrowRight };
