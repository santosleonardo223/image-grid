import React, { Fragment, useState } from 'react';
import { useInfiniteLoader } from 'masonic';

import { Photo, Search, VirtualizedItemProps } from '@src/types';
import { ArrowLeft, ArrowRight, ImageGridInfiniteList } from './styles';
import ImageCell from '@src/components/ImageCell/index';
import Modal from '@src/components/Modal/modal';
import ImageFull from '@src/components/ImageFull/index';
import { CaretCircleDown } from '@src/icons/index';

type ImageGridProps = {
  search?: Search;
  currentPage?: number;
  loadMoreImages: () => void;
};

const ImageGrid = ({ search, currentPage, loadMoreImages }: ImageGridProps) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedPhoto, setSelectedPhoto] = useState<{
    photo: Photo;
    index: number;
  } | null>(null);

  const openModal = (index: number) => (photo: Photo) => () => {
    setSelectedPhoto({ photo, index });
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setSelectedPhoto(null);
    setIsModalOpen(false);
  };

  const moveToPreviousPhoto = () => {
    if (selectedPhoto.index > 0) {
      setSelectedPhoto({
        photo: search.photos[selectedPhoto.index - 1],
        index: selectedPhoto.index - 1,
      });
    }
  };

  const moveToNextPhoto = async () => {
    if (selectedPhoto.index >= search.photos.length - 1) {
      loadMoreImages();
    } else {
      setSelectedPhoto({
        photo: search.photos[selectedPhoto.index + 1],
        index: selectedPhoto.index + 1,
      });
    }
  };

  const checkIfLoadMore = useInfiniteLoader(loadMoreImages, {
    isItemLoaded: (index: number, items: VirtualizedItemProps[]) =>
      !!items[index],
  });

  return (
    <Fragment>
      {search && currentPage ? (
        <ImageGridInfiniteList
          items={search.photos.map((photo, index) => ({
            photo,
            openModal: openModal(index),
          }))}
          onRender={checkIfLoadMore}
          render={ImageCell}
        />
      ) : null}

      {isModalOpen ? (
        <Modal onClose={closeModal}>
          <Fragment>
            {selectedPhoto.index > 0 ? (
              <ArrowLeft onClick={moveToPreviousPhoto}>
                <CaretCircleDown />
              </ArrowLeft>
            ) : null}
            <ImageFull photo={selectedPhoto.photo} />
            {selectedPhoto.index <= search.photos.length ? (
              <ArrowRight onClick={moveToNextPhoto}>
                <CaretCircleDown />
              </ArrowRight>
            ) : null}
          </Fragment>
        </Modal>
      ) : null}
    </Fragment>
  );
};

export default ImageGrid;
