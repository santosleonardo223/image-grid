import styled from 'styled-components';

type FlexProps = {
  height?: string;
  width?: string;
  order?: number;
  flexDirection?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
  flexGrow?: number;
  flexWrap?: 'nowrap' | 'wrap' | 'wrap-reverse';
  flexShrink?: number;
  flexBasis?: string;
  zIndex?: string | number;
  justifyContent?:
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'space-between'
    | 'space-around'
    | 'space-evenly';
  alignSelf?:
    | 'auto'
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'baseline'
    | 'stretch';
  alignItems?: 'stretch' | 'flex-start' | 'flex-end' | 'center' | 'baseline';
  alignContent?:
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'space-between'
    | 'space-around'
    | 'stretch';
};

const Flex = styled.div<FlexProps>`
  display: flex;
  height: ${({ height }) => (height ? `${height}` : 'auto')};
  width: ${({ width }) => (width ? width : '100%')};
  order: ${({ order }) => (order ? order : 0)};
  z-index: ${({ zIndex, theme }) =>
    typeof zIndex !== undefined ? zIndex : theme.zIndex.first};
  flex-direction: ${({ flexDirection }) =>
    flexDirection ? flexDirection : 'row'};
  flex-grow: ${({ flexGrow }) => (flexGrow ? flexGrow : 0)};
  flex-wrap: ${({ flexWrap }) => (flexWrap ? flexWrap : 'nowrap')};
  flex-shrink: ${({ flexShrink }) => (flexShrink ? flexShrink : 1)};
  flex-basis: ${({ flexBasis }) => (flexBasis ? flexBasis : 'auto')};
  justify-content: ${({ justifyContent }) =>
    justifyContent ? justifyContent : 'flex-start'};
  align-self: ${({ alignSelf }) => (alignSelf ? alignSelf : 'auto')};
  align-items: ${({ alignItems }) => (alignItems ? alignItems : 'auto')};
  align-content: ${({ alignContent }) =>
    alignContent ? alignContent : 'auto'};
`;

export { Flex };
