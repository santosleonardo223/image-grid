import styled from 'styled-components';

type GridProps = {
  rows?: number | string;
  columns?: number | string;
  gap?: number | string;
  columnGap?: string;
  rowGap?: string;
  justifyContent?:
    | 'start'
    | 'center'
    | 'space-between'
    | 'space-around'
    | 'space-evenly';
  alignItems?: 'center' | 'start' | 'end' | 'flex-start' | 'flex-end';
  padding?: string;
  width?: string;
  minWidth?: string;
  margin?: string;
};

const Grid = styled.div<GridProps>`
  display: grid;
  grid-template-columns: ${({ columns }) => (columns ? columns : '1')};
  grid-template-rows: ${({ rows }) => (rows ? rows : '1')};
  grid-gap: ${({ gap, theme }) => `${gap ? gap : theme.spacing.xs}px`};
  ${({ columnGap }) => (columnGap ? `column-gap: ${columnGap}` : null)};
  ${({ rowGap }) => (rowGap ? `row-gap: ${rowGap}` : null)};
  ${({ justifyContent }) =>
    justifyContent ? `justify-content: ${justifyContent}` : null};
  ${({ alignItems }) => (alignItems ? `align-items: ${alignItems}` : null)};
  ${({ padding }) => (padding ? `padding: ${padding}` : null)};
  ${({ width }) => (width ? `width: ${width}` : null)};
  ${({ margin }) => (margin ? `margin: ${margin}` : null)};
  ${({ minWidth }) => (minWidth ? `min-width: ${minWidth}` : null)};

  .grid-column-span-2 {
    grid-column: span 2;
  }

  .grid-column-span-4 {
    grid-column: span 4;
  }
`;

export { Grid };
