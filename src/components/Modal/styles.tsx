import styled, { css } from 'styled-components';

import { Flex } from '@src/components/basic';
import theme from '@src/theme/index';

const scrollWidth = theme.spacing.md;
const mobileContentVerticalMargin = theme.spacing.lg;
const mobileContentHorizontalMargin = theme.spacing.sm;
const desktopContentMargin = theme.spacing.xl;
const mobileContentMaxWidth = `calc(100vw - (2 * ${mobileContentHorizontalMargin}) - ${scrollWidth})`;
const desktopContetMaxWidth = `calc(100vw - (2 * ${desktopContentMargin}) - ${scrollWidth})`;
const mobileContentMaxHeight = `calc(100vh - (2 * ${mobileContentVerticalMargin}))`;
const desktopContentMaxHeight = `calc(100vh - (2 * ${desktopContentMargin}))`;

const ModalOverlay = styled(Flex)`
  position: fixed;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100vw;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => `${theme.colors.black}EE`};
  padding-right: ${scrollWidth};

  #closeIcon {
    position: fixed;
    top: ${({ theme }) => theme.spacing.xl};
    right: ${({ theme }) => theme.spacing.xl};
    cursor: pointer;
  }

  #modalContainer svg {
    opacity: 0.1;
    transition: 0.3s ease-in;
  }

  #modalContainer:hover svg {
    opacity: 1;
  }
`;

const ModalContainer = styled.div`
  display: block;
  margin: auto;

  ${({ theme }) => css`
    border-radius: ${theme.borderRadius.default};

    @media screen and (max-width: ${theme.screenBreakPoints.sm}) {
      margin: ${mobileContentVerticalMargin} ${mobileContentHorizontalMargin};
      max-width: ${mobileContentMaxWidth};
      max-height: ${mobileContentMaxHeight};
    }

    @media screen and (min-width: ${theme.screenBreakPoints.sm}) {
      margin: ${desktopContentMargin};
      max-width: ${desktopContetMaxWidth};
      max-height: ${desktopContentMaxHeight};
    }
  `};
`;

export {
  mobileContentMaxWidth,
  desktopContetMaxWidth,
  mobileContentMaxHeight,
  desktopContentMaxHeight,
  ModalOverlay,
  ModalContainer,
};
