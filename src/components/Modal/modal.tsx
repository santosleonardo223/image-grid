import React, { useEffect, useRef } from 'react';
import { createPortal } from 'react-dom';

import { ModalContainer, ModalOverlay } from './styles';
import { Close } from '@src/icons/Close';
import theme from '@src/theme/index';

type ModalProps = {
  children: JSX.Element;
  onClose: () => void;
};

const Modal = ({ children, onClose }: ModalProps) => {
  const ref = useRef<Element | null>(null);

  if (!ref.current) {
    ref.current = document.createElement('div');
  }

  useEffect(() => {
    const rootModal = document.getElementById('root-modal');

    if (rootModal && ref && ref.current) {
      rootModal.appendChild(ref.current);

      return () => {
        if (ref && ref.current) {
          rootModal.removeChild(ref.current);
        }
      };
    }
  }, []);

  return createPortal(
    <ModalOverlay onClick={onClose}>
      <Close id="closeIcon" onClick={onClose} fill={theme.colors.white} />
      <ModalContainer
        id="modalContainer"
        onClick={(e: React.MouseEvent<HTMLElement>) => e.stopPropagation()}
      >
        {children}
      </ModalContainer>
    </ModalOverlay>,
    ref.current
  );
};

export default Modal;
