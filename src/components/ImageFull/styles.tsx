import styled, { css } from 'styled-components';

import {
  desktopContentMaxHeight,
  desktopContetMaxWidth,
  mobileContentMaxHeight,
  mobileContentMaxWidth,
} from '@src/components/Modal/styles';

const ImageFullContainer = styled.div`
  display: block;

  img {
    display: block;
    width: auto;
    height: auto;
  }

  ${({ theme }) => css`
    @media screen and (max-width: ${theme.screenBreakPoints.sm}) {
      img {
        max-width: ${mobileContentMaxWidth};
        max-height: ${mobileContentMaxHeight};
      }
    }

    @media screen and (min-width: ${theme.screenBreakPoints.sm}) {
      img {
        max-width: ${desktopContetMaxWidth};
        max-height: ${desktopContentMaxHeight};
      }
    }
  `};
`;

export { ImageFullContainer };
