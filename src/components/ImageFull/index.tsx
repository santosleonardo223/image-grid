import React from 'react';

import { Photo } from '@src/types';
import { ImageFullContainer } from './styles';

type ImageFullProps = {
  photo: Photo;
};

const ImageFull = ({ photo }: ImageFullProps) => {
  const { urls, alt_description } = photo;

  return (
    <ImageFullContainer>
      <img src={urls.full} alt={alt_description || ''} />
    </ImageFullContainer>
  );
};

export default ImageFull;
