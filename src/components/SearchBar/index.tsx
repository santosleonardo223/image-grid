import React, { useState } from 'react';

import { SearchContainer } from './styles';
import { Search, Spinner } from '@src/icons/index';

type SearchBarProps = {
  placeholder?: string;
  handleSubmit: (
    search: string
  ) => (e?: React.FormEvent<HTMLFormElement>) => void;
  isLoading: boolean;
};

const SearchBar = ({
  placeholder,
  handleSubmit,
  isLoading,
}: SearchBarProps) => {
  const [search, setSearch] = useState('');

  return (
    <SearchContainer onSubmit={handleSubmit(search)} isLoading={isLoading}>
      <input
        type="text"
        name="search"
        placeholder={placeholder}
        value={search}
        onChange={(e) => {
          setSearch(e.target.value);
        }}
        disabled={isLoading}
      />
      {isLoading ? <Spinner /> : <Search onClick={handleSubmit(search)} />}
    </SearchContainer>
  );
};

export default SearchBar;
