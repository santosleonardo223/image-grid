import styled from 'styled-components';

const SearchContainer = styled.form<{ isLoading: boolean }>`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  max-width: ${({ theme }) => theme.screenBreakPoints.sm};
  padding: ${({ theme }) => `${theme.spacing.sm} ${theme.spacing.md}`};
  margin: auto;
  background-color: ${({ theme, isLoading }) =>
    isLoading ? theme.colors.lightGray : theme.colors.white};
  border: ${({ theme }) =>
    `${theme.borders.default} solid ${theme.colors.gray}`};
  border-radius: ${({ theme }) => theme.borderRadius.default};

  input {
    border: none;
    flex-grow: 1;
    font-size: ${({ theme }) => theme.typography.h2.fontSize};
    line-height: ${({ theme }) => theme.typography.h2.lineHeight};
    font-weight: ${({ theme }) => theme.typography.h2.fontWeight};
  }

  input:focus {
    outline: none;
  }

  input:disabled {
    background-color: ${({ theme }) => theme.colors.lightGray};
  }

  svg {
    margin-left: ${({ theme }) => theme.spacing.md};
    fill: ${({ theme }) => theme.colors.gray};
    cursor: pointer;
  }

  svg:hover {
    fill: ${({ theme }) => theme.colors.black};
  }
`;

export { SearchContainer };
