import styled from 'styled-components';

const ImageContainer = styled.div`
  align-items: stretch;

  img {
    cursor: pointer;
  }
`;

export { ImageContainer };
