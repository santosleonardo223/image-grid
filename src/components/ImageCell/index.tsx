import React from 'react';

import { Photo } from '@src/types';
import { ImageContainer } from './styles';

type ImageCellProps = {
  index: number;
  data: {
    photo: Photo;
    openModal: (photo: Photo) => () => void;
  };
  width: number;
};

const ImageCell = ({ index, data: { photo, openModal } }: ImageCellProps) => {
  const {
    id,
    urls: { regular },
    alt_description,
  } = photo;

  return (
    <ImageContainer key={`${id}_${index}`}>
      <img
        src={regular}
        alt={alt_description || ''}
        loading="lazy"
        onClick={openModal(photo)}
      />
    </ImageContainer>
  );
};

export default ImageCell;
