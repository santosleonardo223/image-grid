import React, { Fragment, useEffect, useState } from 'react';
import Unsplash, { toJson } from 'unsplash-js';

import { MAX_IMAGES_PER_PAGE } from '@src/constants/index';
import { Search } from '@src/types';
import SearchBar from '@src/components/SearchBar/index';
import ImageGrid from '@src/components/ImageGrid/index';

const ImageGallery = () => {
  const unsplash = new Unsplash({
    accessKey: process.env.UNSPLASH_KEY,
  });
  const [currentPage, setCurrentPage] = useState<number>(undefined);
  const [search, setSearch] = useState<Search>(undefined);
  const [searchText, setSearchText] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isFetchingMoreData, setIsFetchingMoreData] = useState(false);

  const loadMoreImages = () => {
    if (!isFetchingMoreData) {
      setIsFetchingMoreData(true);
    }
  };

  useEffect(() => {
    if (isFetchingMoreData) {
      unsplash.search
        .photos(searchText, currentPage + 1, MAX_IMAGES_PER_PAGE)
        .then(toJson)
        .then(({ results }) => {
          setSearch((prevState) => ({
            ...prevState,
            photos: [...prevState.photos, ...results],
          }));
        })
        .catch((err) => console.error(err))
        .finally(() => {
          setCurrentPage((prevState) => (prevState ?? 0) + 1);
          setIsFetchingMoreData(false);
        });
    }
  }, [isFetchingMoreData]);

  const handleRequestImages = (searchText: string) => (
    e?: React.FormEvent<HTMLFormElement>
  ) => {
    if (e) {
      e.preventDefault();
    }

    setIsLoading(true);
    setCurrentPage(undefined);
    unsplash.search
      .photos(searchText, currentPage, MAX_IMAGES_PER_PAGE)
      .then(toJson)
      .then(({ results, total, total_pages }) => {
        setSearch({
          photos: results ? results : [],
          total: total ? total : 0,
          total_pages: total_pages ? total_pages : 0,
        });
        setCurrentPage(1);
      })
      .catch((err) => console.error(err))
      .finally(() => {
        setSearchText(searchText);
        setIsLoading(false);
      });
  };

  return (
    <Fragment>
      <SearchBar
        placeholder="Search an Image"
        handleSubmit={handleRequestImages}
        isLoading={isLoading}
      />
      <ImageGrid
        search={search}
        currentPage={currentPage}
        loadMoreImages={loadMoreImages}
      />
    </Fragment>
  );
};

export default ImageGallery;
