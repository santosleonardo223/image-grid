const theme = {
  colors: {
    black: '#000000',
    white: '#FFFFFF',
    lightGray: '#F0F1F2',
    gray: '#808080',
  },
  widths: {
    icon: '24px',
  },
  heights: {
    icon: '24px',
  },
  borders: {
    default: '1px',
  },
  borderRadius: {
    default: '4px',
  },
  spacing: {
    xs: '4px',
    sm: '8px',
    md: '16px',
    lg: '24px',
    xl: '32px',
  },
  zIndex: {
    first: 1,
  },
  screenBreakPoints: {
    zero: '0px',
    xs: '450px',
    sm: '600px',
    md: '750px',
    lg: '960px',
    xl: '1280px',
  },
  typography: {
    h2: {
      fontSize: '20px',
      lineHeight: '28px',
      fontWeight: '500',
    },
  },
};

export default theme;
