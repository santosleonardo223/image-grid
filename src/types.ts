export type VirtualizedItemProps = {
  photo: Photo;
  openModal: (photo: Photo) => () => void;
};

export type PhotoUrl = {
  full: string;
  regular: string;
  small: string;
  thumb: string;
};

export type Photo = {
  id: string;
  width: number;
  height: number;
  description: string | null;
  alt_description: string | null;
  urls: PhotoUrl;
};

export type Search = {
  photos: Photo[];
  total: number;
  total_pages: number;
};
