# SETUP BEFORE RUNNING PROJECT
Before you're able to run the project, you'll need to setup the unsplash access key.
It should be done by following these steps:

  1. Create a file called ".env" in the root of the project;
  2. Inside the file, add the follwoing line: 
     UNSPLASH_KEY=abc

     where "abc" is a valid unsplash access key. 


# HOW TO RUN
Once the unsplash key setup is done, you'll need to download the project dependencies,
build it, and then run. This can be done by running the following commands:

yarn
yarn build
yarn start